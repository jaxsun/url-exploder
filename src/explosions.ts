const prefixes = ["", "KA", "KER"];
const sounds = ["BOOM", "POW", "BLAM", "BLAMMO", "THOOM", "BAM"];

export function generateExplosion(): string {
    var prefix = getRandomElement(prefixes);
    var sound = getRandomElement(sounds);
    var exclamations = "!".repeat(randomInt(5));
    var emojis = "💥".repeat(randomInt(3));
    return `${emojis}${prefix}${sound}${exclamations}${emojis}`;
}

function getRandomElement<T>(list: T[]) {
    return list[randomInt(list.length)];
}

function randomInt(max: number): number {
    return Math.floor(Math.random() * max);
}