import { URL } from 'url';
import * as UrlUtils from './url-utils';
import * as Explosions from './explosions';
import * as vscode from 'vscode';

export function activate(context: vscode.ExtensionContext) {

	let disposable = vscode.commands.registerTextEditorCommand('url-exploder.explode', (editor: vscode.TextEditor, edit: vscode.TextEditorEdit) => {
		const selectedText = editor.document.getText(editor.selection);
		const url = UrlUtils.maybeParseUrl(selectedText);
		if (url === null) {
			vscode.window.showInformationMessage(`Could not parse URL from selected text: ${selectedText}`);
		} else {
			const exploded = UrlUtils.explode(url);
			editor.edit(editBuilder => {
				editBuilder.insert(editor.selection.end, `\n\n${exploded}`);
			});
			vscode.window.showInformationMessage(Explosions.generateExplosion());
		}
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() { }
