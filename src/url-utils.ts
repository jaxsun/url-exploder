import { URL } from 'url';

export function explode(url: URL): string {
    let maxNameLength = 0;
    url.searchParams.forEach((value: string, name: string) => {
        if (name.length > maxNameLength) {
            maxNameLength = name.length;
        }
    });
    let output = `${url.protocol}//${url.host}${url.pathname}`;
    url.searchParams.forEach((value: string, name: string) => {
        output += `\n${name.padEnd(maxNameLength, " ")}: ${value}`;
    });
    return output;
}

export function maybeParseUrl(text: string): URL | null {
    const lines = text.split("\n").filter(line => line.length > 0);
    const firstLine = lines.length > 0 ? lines[0] : "";
    try {
        return new URL(firstLine);
    } catch (e) {
        return null;
    }
}