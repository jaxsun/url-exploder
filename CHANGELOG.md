# Change Log

All notable changes to the "url-exploder" extension will be documented in this file.

## [1.0.0]

- Initial release.

## [1.0.1]

- Added 💥 emoji to explosion messages.

## [1.0.2]

- Added right click context menu entry
- Improved error handling
- Improved handling of multiline strings
- Added more explosion sounds
- Added changelog