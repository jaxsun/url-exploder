# url-exploder README

## Features

Disassembles and decodes a URL to easily inspect URL parameters.

![Explosions](./images/url-exploder.gif)